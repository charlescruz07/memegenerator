package com.example.acer.memegenerator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MemeInput extends AppCompatActivity {

    private EditText topTxt, botTxt;
    private Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meme_input);

        topTxt = (EditText) findViewById(R.id.edTop);
        botTxt = (EditText) findViewById(R.id.edBot);
        btnCreate = (Button) findViewById(R.id.btnCreate);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MemeInput.this, MemeView.class);
                String txt1 = topTxt.getText().toString();
                String txt2 = botTxt.getText().toString();
                intent.putExtra("top",txt1);
                intent.putExtra("bot",txt2);
                startActivity(intent);
            }
        });
    }
}
