package com.example.acer.memegenerator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MemeView extends AppCompatActivity {

    private TextView txtTop,txtBot;
    private String txt1,txt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meme_view);

        txtTop = (TextView) findViewById(R.id.txtTop);
        txtBot = (TextView) findViewById(R.id.txtBottom);
        Intent intent = getIntent();
        txt1 = intent.getStringExtra("top");
        txt2 = intent.getStringExtra("bot");
        txtTop.setText(txt1);
        txtBot.setText(txt2);

    }
}
